import { StyleSheet, Text, View, Button } from 'react-native'
import React from 'react'

export function HomeScreen({navigation}) {
  return (
    <View style={styles.container}>
      <Text>Tela Inicial</Text>
      <Button
        title='Ir para Segunda Tela'
        onPress={() => navigation.navigate('SegundaTela')}
      />
    </View>
  )
}

const styles = StyleSheet.create({
        container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        },
    });