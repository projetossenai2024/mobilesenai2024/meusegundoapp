import {useState} from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import Layout from './layoutEstrutura';

export default function LayoutHorizontal() {

  const [items, setItems] = useState([1]);

  const adicionarItem = () => {
    const novoItem = items.length + 1;
    setItems([...items, novoItem]);
  };

  const removerItem = () => {
    if (items.length > 0) {
      setItems(items.slice(0, -1)); // Remove o último item do array
    }
  };
  
  
  return (
    <View style={styles.container}>
      <ScrollView horizontal={true} style={styles.scroll}>
        {items.map((id, index) => (
          <View style={styles.box1} key={index}>
            <Text>{id}</Text>
          </View>
        ))}
      </ScrollView>
      <View style={{flexDirection: 'row'}}>
          <TouchableOpacity style={{backgroundColor: '#fff', alignSelf: 'center'}} onPress={adicionarItem}>
              <View style={styles.box2}></View>
          </TouchableOpacity>
          <TouchableOpacity style={{backgroundColor: '#fff', alignSelf: 'center'}} onPress={removerItem}>
              <View style={styles.box3}></View>
          </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'flex-start',
    flexDirection: 'column',
  },
  scroll: {
    marginTop: 40,
    maxHeight: 70,
  },
  box1: {
    width: 64,
    height: 64,
    backgroundColor: 'blue',
    marginRight: 10,
    borderRadius: 32,
  },
  box2: {
    width: 64,
    height: 64,
    backgroundColor: 'green',
    marginRight: 10,
    borderRadius: 32,
  },
  box3: {
    width: 64,
    height: 64,
    backgroundColor: 'red',
    marginRight: 10,
    borderRadius: 32,
  },
});
