//import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';

export function Menu({navigation}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.menu}
        onPress={()=> navigation.navigate('HomeScreen')}
      >
        <Text>Opção 1</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}
        onPress={()=> navigation.navigate('AnotherScreen')}
      >
        <Text>Opção 2</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}
      onPress={()=> navigation.navigate('AnotherScreen')}
      >
        <Text>Opção 3</Text>
      </TouchableOpacity>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  menu:{
    padding: 10,
    margin: 5,
    backgroundColor: 'lightblue',
    borderRadius: 8,
  }
});
