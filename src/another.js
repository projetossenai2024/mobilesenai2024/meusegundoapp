import { StyleSheet, Text, View, Button } from 'react-native'
import React from 'react'

export function AnotherScreen({navigation}) {
  return (
    <View style={styles.container}>
      <Text>Sim, essa é a segunda tela, gostou? macaco curupira</Text>
      <Button
        title='essa é a segunda tela'
        onPress={() => navigation.goBack()}
      />
    </View>
  )
}

const styles = StyleSheet.create({
        container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        },
    });